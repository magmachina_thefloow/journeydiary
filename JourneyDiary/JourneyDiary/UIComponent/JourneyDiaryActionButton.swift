//
//  JourneyManagerActionButton.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 30/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit

/**
 The button at the bottom of the map view.
 If we are showing a recorded journey it let us start stop the path animation.
 If we are ready to record a new journey it let us start stop tracking the user path
 ## Discussion ##
 The play mode will be available in the future. For now we simply hide the button when showing a recorded journey
 
 */
class JourneyDiaryActionButton: UIButton {

    /// The possibles kind of button. It changes color if it is record or play
    ///
    /// - Record: it will be styled as a record button
    /// - Play: it will be styled as a play button
    public enum Kind{
        case Record
        case Play
    }
    
    /// The possible statuses of the button. It determines the text shown in the title
    ///
    /// - ReadyToRecord: Ready to record a journey
    /// - Recording: Recording a journey
    /// - ReadyToPlay: Ready to play a recorded journey
    /// - Playing: Playing a recorded journey
    enum ActionButtonStatus {
        case ReadyToRecord
        case Recording
        case ReadyToPlay
        case Playing
    }
    
    private var _kind:Kind = .Record
    private var _status:ActionButtonStatus = .ReadyToRecord
    
    /// The kind of button
    public var kind:Kind {
        get {
            return _kind
        }
        
        set(value) {
            self._kind = value
            configure()
        }
    }
    
    /// the status of the button
    public var status:ActionButtonStatus {
        get {
            return _status
        }
        
        set(value) {
            self._status = value
            configure()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 11
        self.layer.masksToBounds = true
        self.configure()
    }
    
    /// Starts a pulse anumation used while recording
    public func startPulsing() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 1.2
        flash.fromValue = 1
        flash.toValue = 0.75
        flash.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = Float.infinity
        
        layer.add(flash, forKey: "flash")
    }
    
    /// Stops the pulsing animation
    public func stopPulsing() {
        layer.removeAnimation(forKey: "flash")
    }
    
    /// It applies the style to the button depending on its status and kind.
    private func configure() {
        switch self._kind {
        case .Record:
            self.backgroundColor = Constants.RECORD_COLOR
            break
        case .Play:
            self.backgroundColor = Constants.PLAY_COLOR
            break
        }
        
        switch self._status {
        case .ReadyToRecord:
            self.setTitle(Constants.LS("startTracking"), for: .normal)
            break
        case .Recording:
            self.setTitle(Constants.LS("stopTracking"), for: .normal)
            break
        case .ReadyToPlay:
            self.setTitle(Constants.LS("play"), for: .normal)
            break
        case .Playing:
            self.setTitle(Constants.LS("stop"), for: .normal)
            break
        }
    }
    
}

//
//  Journey.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 28/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit
import MapKit

/**
 A journey with its own title, description, and its list of points.
 Also you can define the transportType
 
 ## Discussion ##
 Description is empty now, but in the future we could give the user the ability to assign these pros and modfiy them anytime.
 We can also imagine this as a journey segment.
 A journey could be mad of several segments (each one of them with their start and end time, and transportation type.
 The user should not only record but also resume a journey if he wants.
 A journey should be stored only if it has >1 points.
 */
class Journey:Codable {
    
    private var _uid: String
    private var _title: String
    private var _description: String
    private var _points: [JourneyPoint]
    private var _time:Double
    private var _transportType:MKDirectionsTransportType
    
    enum CodingKeys: String, CodingKey {
        case _uid
        case _title
        case _description
        case _points
        case _time
        case _transportType
    }
    
    /// The unique identifier of the Journey.
    var uid: String {
        get {
            return _uid
        }
    }
    
    /// The title of the Journey.
    var title: String {
        get {
            return _title
        }
        
        set(value) {
            _title = value
        }
    }
    
    /// The brief description of the Journey.
    var description: String {
        get {
            return _description
        }
        
        set(value) {
            _description = value
        }
    }
    
    /// The time the journey ws registered.
    var time: Date {
        get {
            return Date(timeIntervalSince1970: _time)
        }
    }
    
    
    /// the JourneyPoints of the path.
    var points: [JourneyPoint] {
        get {
            return _points
        }
    }
    
    
    /// The start date of the journey
    var startDate:Date? {
        get {
            if let p0:JourneyPoint = self.points.first {
                return p0.time
            }
            return self.time
        }
    }
    
    /// The end date of the journey
    var endDate:Date? {
        get {
            if let p1:JourneyPoint = self.points.last {
                return p1.time
            }
            return self.time
        }
    }
    
    /// The distance covered, in meters
    /**
    ## Discussion ##
    distance should be calculated during recording
    */
    var distanceCovered:Double {
        get {
            if (self.points.count > 0) {
                var distanceResult:Double = 0
                for i in 1..<self.points.count {
                    let p0:MKMapPoint = MKMapPointForCoordinate(self.points[i-1].coordinate)
                    let p1:MKMapPoint = MKMapPointForCoordinate(self.points[i].coordinate)
                    distanceResult += MKMetersBetweenMapPoints(p0, p1)
                }
                return distanceResult
            }
            return 0
        }
    }
    
    /// The duration of the journey, in seconds
    var duration:Double {
        get {
            if let d0:Date = self.startDate {
                if let d1:Date = self.endDate {
                    return d1.timeIntervalSince1970 - d0.timeIntervalSince1970
                }
            }
            return 0
        }
    }
    
    /// It initializes a Journey
    ///
    /// - Parameters:
    ///   - title: the Journey title.
    ///   - transportType: the transport type.
    /**
     ## Discussion ##
     The transport type is set to its default value, but the user should define it in the future.
     */
    init(title: String, transportType: MKDirectionsTransportType = .any) {
        self._uid = UUID().uuidString
        self._title = title
        self._description = ""
        self._time = NSDate().timeIntervalSince1970
        self._transportType = transportType
        self._points = [JourneyPoint]()
    }
    
    /// It adds a journeyPoint to the journey
    ///
    /// - Parameter value: the JourneyPoint to add.
    func addPoint(_ value:JourneyPoint) {
        self._points.append(value);
    }
    
    //MARK: Codable conformance
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self._uid = try values.decode(String.self, forKey: ._uid)
        self._title = try values.decode(String.self, forKey: ._title)
        self._description = try values.decode(String.self, forKey: ._description)
        self._time = try values.decode(Double.self, forKey: ._time)
        self._points = try values.decode([JourneyPoint].self, forKey: ._points)
        let transportType:Int = try values.decode(Int.self, forKey: ._transportType)
        switch transportType {
        case 0:
            self._transportType = .automobile
            break
        case 1:
            self._transportType = .transit
            break
        case 2:
            self._transportType = .walking
            break
        default:
            self._transportType = .any
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(_uid, forKey: ._uid)
        try container.encode(_title, forKey: ._title)
        try container.encode(_description, forKey: ._description)
        try container.encode(_time, forKey: ._time)
        try container.encode(_points, forKey: ._points)
        var transportType:Int
        switch _transportType {
        case .automobile:
            transportType = 0
            break
        case .transit:
            transportType = 1
            break
        case .walking:
            transportType = 2
            break
        default:
            transportType = -1
        }
        try container.encode(transportType, forKey: ._transportType)
    }
}


//
//  JourneyPoint.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 28/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit
import CoreLocation

/**
 A point along a journey with its own coordinate, time and a unique id.
 
 ## Discussion ##
 We should use the unique id, in the future, to bind some media (text notes, photos, short videos) to a specific point.
 
 */
struct JourneyPoint:Codable {
    
    private var _uid: String
    private var _latitude: Double
    private var _longitude: Double
    private var _elevation: Double
    private var _time: Double

    
    /// The unique identifier of the point
    var uid: String {
        get {
            return _uid
        }
    }
    
    /// The latitude and longitude of the point
    var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: _latitude, longitude: _longitude)
        }
    }
    
    /// The (eventual) elevation of the point
    var elevation: Double {
        get {
            return _elevation
        }
    }
    
    /// The time the journeyPoint was registered
    var time: Date {
        get {
            return Date(timeIntervalSince1970: _time)
        }
    }
    
    /// It initializes a JourneyPoint.
    ///
    /// - Parameters:
    ///   - latitude: the latitude of the point.
    ///   - longitude: the longitude of the point.
    ///   - elevation: (optional) the eventual elevation of the point.
    init(latitude: Double, longitude: Double, elevation:Double = 0) {
        
        self._uid = UUID().uuidString
        self._latitude = latitude
        self._longitude = longitude
        self._elevation = elevation
        self._time = Date().timeIntervalSince1970
  
    }

}



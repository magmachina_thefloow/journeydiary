//
//  Constants.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 31/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

/**
 This is just a place to centralize the constants used in the app
 
 */
class Constants {
    /// The color of the recording phase (path and record button)
    static let RECORD_COLOR:UIColor = UIColor(red: 227/255, green: 90/255, blue: 102/255, alpha: 1)
    
    /// The color of the playing/showing phase of  arecorded journey (path and play button)
    static let PLAY_COLOR:UIColor = UIColor(red: 97/255, green: 181/255, blue: 235/255, alpha: 1)
    
    /// The desidered accuracy of the location manager
    /**
     ## Discussion ##
     The app should have a setting section to let the user choose, also we can define the accuracy depending on transport type, and speed.
     */
    static let DESIDERED_ACCURACY:CLLocationAccuracy = kCLLocationAccuracyBest
    
    /// The minimum distance to store a new point
    static let MINIMUM_DELTA_METERS = 10.0
    
    /// The cell identifier when presenting journeys list
    static let JOURNEY_CELL_IDENTIFIER = "JourneyCell"
    
    /// The locale to use (for instance in formatting dates)
    static let LOCALE:String = "en_US"
    
    /// The map zoom
    /**
     ## Discussion ##
     Should be in settings or depending on the current speed.
     */
    static let MK_MAP_ZOOM: CLLocationDistance = 200
    
    /// The offset around the path to see it well
    static let MK_MAP_WHOLE_PATH_OFFSET: Double = 700
    
    /// The duration of play animation, in seconds
    /**
     ## Discussion ##
     Should be definitively in settings or a multiplyer factor of the originl duration of that specific journey.
     */
    static let PLAY_ANIMATION_DURATION: TimeInterval = 15
    
    /// The framerate of play animation, in seconds
    static let PLAY_ANIMATION_FRAMERATE: TimeInterval = 0.1
    
    /// It works like a shortcut for NSLocalizedString
    ///
    /// - Parameter key: the localized key
    /// - Returns: the resulting string
    static func LS(_ key:String) -> String {
        return NSLocalizedString(key, comment: "")
    }

}


//
//  JourneyManager.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 29/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import Foundation

/// A singleton manager which stores and retrieves user journeys.
class JourneyManager {
    static let sharedInstance = JourneyManager()
    
    private var journeysPath:String = "journeys.json"
    private var _journeys:[Journey]
    private var _journeysIndexes:[String: Int]
    
    /// the user journeys
    var journeys:[Journey] {
        get {
            return self._journeys
        }
    }
    
    /// It initializes the instance the first and only time it is created
    private init() {
        
        //Storage.clear(.documents) // here just for testing phase purpose
        
        self._journeysIndexes = [String:Int]()
        do {
            let journeys:[Journey] = try Storage.retrieve(journeysPath, from: .documents, as: [Journey].self)
           
            //sorting recorded journeys from the most recent to the oldest
            self._journeys = journeys.sorted { $0.startDate!.timeIntervalSince1970 > $1.startDate!.timeIntervalSince1970 }
            
            //Filling the _journeysIndexes dictionary to quickly retrieve a journey by its unique id
            for i in 0..<self._journeys.count {
                self._journeysIndexes[self._journeys[i].uid] = i
            }
        } catch {
            self._journeys = [Journey]()
        }
    }
    
    /// Adds a new journey and store it to the device documents folder
    ///
    /// - Parameter value: the journey to create and store.
    func AddJourney(_ value:Journey) {
        //Adding the journey to the _journeysIndexes dictionary to quickly retrieve it by its unique id
        self._journeysIndexes[value.uid] = self._journeys.count
        //appending the journey to the arrey
        self._journeys.insert(value, at: 0)
        self.SaveJourneys()
    }
    
    /// Returns the journey with that unique id, if exists
    ///
    /// - Parameter value: the unique id of the journey
    func JourneyById(_ value:String) -> Journey? {
        if (!self._journeysIndexes.keys.contains(value)) {
            return nil
        }
        return self._journeys[self._journeysIndexes[value]!]
    }
    
    /// Stores the journeys on the device
    private func SaveJourneys() {
        do {
            try Storage.store(self._journeys, to: .documents, as: journeysPath)
        } catch {
            //TODO: Handle the error
        }
    }
    
}

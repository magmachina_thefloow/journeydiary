//
//  ViewController.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 28/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit
import MapKit

/**
 This is the base map controller.
 It just handles all the locationManager authorizationStatus flow.
 It also shows the user position on the map
 When the app goes to background, showsUserLocation is set to false avoiding location to update.
 When app moved to foreground it is set back to true.
 */
class MapController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    
    @objc internal func appMovedToBackground() {
        self.mapView.showsUserLocation = false
    }

    @objc internal func appMovedToForeground() {
        self.mapView.showsUserLocation = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        //subscribing on appear to notification center, to be notified when app goes to background and foreground
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //unsubscribing notificationCenter on disappear
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mapView.isHidden = true
        enableLocationServices()
    }
    
    internal func enableLocationServices() {
        
        locationManager.delegate = self
        
        //the desired accuracy should be defined in the settings section of the app
        locationManager.desiredAccuracy = Constants.DESIDERED_ACCURACY
        
        checkAuthorizationStatus(CLLocationManager.authorizationStatus())
    }
    
    //used to try the escalation accoridng to https://developer.apple.com/documentation/corelocation/choosing_the_authorization_level_for_location_services/requesting_always_authorization
    internal func escalateLocationServiceAuthorization() {
        // Escalate only when the authorization is set to when-in-use
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    //disabling specific fetures if the CLAuthorizationStatus is .denied or .restricted
    internal func disableMyLocationBasedFeatures() {
        self.mapView.showsUserLocation = false
        self.mapView.isHidden = true
        self.showAlertAboutLocationSettings()
    }
    
    //enabling specific fetures if the CLAuthorizationStatus is .whenInUse
    internal func enableMyWhenInUseFeatures() {
        self.mapView.showsUserLocation = true
        self.mapView.isHidden = false
    }
    
    //enabling specific fetures if the CLAuthorizationStatus is .always
    internal func enableMyAlwaysFeatures() {
        self.mapView.showsUserLocation = true
        self.mapView.isHidden = false
    }
    
    //if CLAuthorizationStatus is .denied or .restricted we tell the user JourenyDiary can't run and invite him/her to go to the settings and change his/her preference
    internal func showAlertAboutLocationSettings() {
        let alertController = UIAlertController (title: Constants.LS("locationSettingDisabledTitle"), message: Constants.LS("locationSettingDisabledMsg"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.LS("goToSettings"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    //print("Settings opened: \(success)")
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: Constants.LS("later"), style: .default, handler:{ (_) in
            self.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Location Manager Delegate
    //handling change of status
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        
        checkAuthorizationStatus(status)
    }
    
    //Checking CLAuthorizationStatus according to https://developer.apple.com/documentation/corelocation/choosing_the_authorization_level_for_location_services/requesting_always_authorization
    func checkAuthorizationStatus(_ status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // Request when-in-use authorization initially
            
            //According to what I understand at https://developer.apple.com/documentation/corelocation/choosing_the_authorization_level_for_location_services/requesting_always_authorization
            // In iOS 11 and later, if you initially requested only when-in-use authorization, you can call the requestAlwaysAuthorization() method at a later time.
            // It sounds like that only from iOS 11 on.
            
            if #available(iOS 11, *) {
                locationManager.requestWhenInUseAuthorization()
            }
            else {
                locationManager.requestAlwaysAuthorization()
            }
            
            break
            
        case .restricted, .denied:
            // Disable location features
            disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            enableMyWhenInUseFeatures()
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            enableMyAlwaysFeatures()
            break
        }
    }
}

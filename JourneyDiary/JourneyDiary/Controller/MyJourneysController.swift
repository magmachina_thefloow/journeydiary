//
//  MyJourneysController.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 31/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit

/// The controller where the user journeys are presented
/**
 ## Discussion ##
 The user should be able also to delete them from here
 */
class MyJourneysController: UIViewController {
    
    //The delegate and dataSource of tableView are defined in InterfaceBuilder,
    //so are defined the segues from each cell and the addJourneyButton, which the user clicks to create a brand new journey
    //(see Main.storyboard/MyJourneysController)
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var addJourneyButton:UIButton!
    @IBOutlet weak var noJourneyLabel:UILabel!
    
    //the current journey when selected from a cell
    private var currentJourney:Journey?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addJourneyButton.layer.cornerRadius = 11
        self.addJourneyButton.layer.masksToBounds = true
        self.noJourneyLabel.text = Constants.LS("noJourneysMsg")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.noJourneyLabel.isHidden = JourneyManager.sharedInstance.journeys.count>0
        self.tableView.isHidden = JourneyManager.sharedInstance.journeys.count==0
        //If we just came back from recording a new journey
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //If the user clicks the button, currentJourney goes to nil
        if let btn:UIButton = sender as? UIButton {
            if (btn == self.addJourneyButton){
                self.currentJourney = nil
            }
        }
        
        //set journey to segue.destination
        if let journeyMap:JourneyDiaryMapController = segue.destination as? JourneyDiaryMapController {
            journeyMap.journey = self.currentJourney
        }
    }

}


//MARK: TableDataSource ad TableView delegate

extension MyJourneysController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.JOURNEY_CELL_IDENTIFIER) else {
                return UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: Constants.JOURNEY_CELL_IDENTIFIER)
            }
            return cell
        }()
        
        return cell
    }
    
    //I prefer to use this method to build the configure to display, leaving "cellForRowAt" just to return the new or reused cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: Constants.LOCALE)
       
        let journey:Journey = JourneyManager.sharedInstance.journeys[indexPath.row]
        
        var detailString:String = ""
        detailString.append(Constants.LS("startDate") + ": "
            + dateFormatter.string(from: journey.startDate!) + "\n")
        detailString.append(Constants.LS("endDate") + ": "
            + dateFormatter.string(from: journey.endDate!) + "\n")
        detailString.append(Constants.LS("distance") + ": "
            + String(format: "%.2f", journey.distanceCovered) + " " + Constants.LS("meters"))
        
        cell.textLabel?.text = journey.title
        cell.detailTextLabel?.text = detailString
    }
    
    //when I'm selecting a row, currentJourney is set
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.currentJourney = JourneyManager.sharedInstance.journeys[indexPath.row]
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JourneyManager.sharedInstance.journeys.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
    
}

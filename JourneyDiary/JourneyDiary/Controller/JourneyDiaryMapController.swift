//
//  JourneyDiaryMapController.swift
//  JourneyDiary
//
//  Created by Mariano Cigliano on 31/05/18.
//  Copyright © 2018 magmachina. All rights reserved.
//

import UIKit
import MapKit

/**
 The evolution of MapController
 In addition to follow the user on the map, it lets you track (grab) the user path and, depending on the user permission, keep on tracking in background mode.
 When the user stops tracking the controller shows the last recorded path
  ## Discussion ##
 The user should be able also to enable heading
 */
class JourneyDiaryMapController: MapController, MKMapViewDelegate {
    
    /// The possible statuses of the controller.
    ///
    /// - ReadyToRecord: Ready to record a journey
    /// - Recording: Recording a journey
    /// - ReadyToPlay: Ready to play a recorded journey
    /// - Playing: Playing a recorded journey
    enum Status {
        case ReadyToRecord
        case Recording
        case ReadyToPlay
        case Playing
        case None
    }
    
    /// the status of the controller
    internal var status:Status = .None {
        didSet {
            switch status {
            case .ReadyToRecord:
                //action button is in recordmode
                self.actionButton.kind = .Record
                self.actionButton.status = .ReadyToRecord
                self.actionButton.isHidden = false
                self.mapView.showsUserLocation = true
                break
            case .Recording:
                self.actionButton.kind = .Record
                self.actionButton.status = .Recording
                self.actionButton.isHidden = false
                self.mapView.showsUserLocation = true
                break
            case .ReadyToPlay:
                self.actionButton.kind = .Play
                self.actionButton.status = .ReadyToPlay
                self.actionButton.isHidden = false
                self.mapView.showsUserLocation = false
                self.drawPolyline()
                break
            case .Playing:
                self.actionButton.kind = .Play
                self.actionButton.status = .Playing
                self.actionButton.isHidden = false
                self.mapView.showsUserLocation = false
                break
            case .None:
                break
            }
        }
    }
    
    /* This button is styled depending on checkStatus method logic
       It can appear as a record button or Play button
       The play feature is hidden for now
     */
    @IBOutlet weak var actionButton:JourneyDiaryActionButton!
    
    //Is true when we are recording a journey
    internal var isRecording:Bool = false
    
    //Is true when we are playing back a recorded journey
    internal var isPlaying:Bool = false
    
    //the play animation cursor
    internal var animationTimer:Timer?
    
    //the play animation start time
    internal var animationTime:TimeInterval = 0
    
    //Is true if the region is changing (to avoid conflict between zoomOnUser and zoom gesture
    internal var regionIsChanging:Bool = false
    
    //the current user location
    internal var currentUserLocationCoordinate:CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    
    //The current journey. Is null when we start a new one
    internal var journey:Journey?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.actionButton.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //stops locationManager from updating, just in case
        locationManager.stopUpdatingLocation()
        //stops eventual play animation
        if let t:Timer = self.animationTimer {
            t.invalidate()
        }
    }
    
    //Override the mapController method
    @objc override internal func appMovedToBackground() {
        super.appMovedToBackground()
        
        //if we are recording, but going to background
        if(self.isRecording){
            //stop actionButton animation
            self.actionButton.stopPulsing()
        }else{
            locationManager.stopUpdatingLocation()
        }
    }
    
    //Override the mapController method
    @objc override internal func appMovedToForeground() {
        checkMyStatus()
        
        //If we are recording, and going back to foreground
        if(self.isRecording) {
            //resume the actionButton animation
            self.actionButton.startPulsing()
        } else{
            locationManager.startUpdatingLocation()
        }
    }
    
    override internal func enableLocationServices() {
        //if we are showing a recorded journey, we don't need to enableLocationServices
        if let journey:Journey = self.journey {
            if (journey.points.count>0){
                self.checkMyStatus()
                return
            }
        }
        locationManager.pausesLocationUpdatesAutomatically = true
        super.enableLocationServices()
    }
    
    override internal func enableMyWhenInUseFeatures() {
        locationManager.startUpdatingLocation()
        checkMyStatus()
    }
    
    override internal func enableMyAlwaysFeatures() {
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        checkMyStatus()
    }
    
    /// Adds a point to the journey and updates the polyline
    ///
    /// - Parameter value: the coordinate of the point
    func addRecordCoordinate (_ value:CLLocationCoordinate2D) {
        
        //we set this to true in case this is the first point we add
        var distanceIsEnough:Bool = true
        
        //if we have already points
        if (self.journey!.points.count>0){
            
            //we check if the distance si enough to add a new one
            let lastCoordinate = self.journey!.points.last!.coordinate
            let lastMapPoint:MKMapPoint = MKMapPointForCoordinate(lastCoordinate)
            let currentMapPoint:MKMapPoint = MKMapPointForCoordinate(value)
            distanceIsEnough = MKMetersBetweenMapPoints(lastMapPoint, currentMapPoint) > Constants.MINIMUM_DELTA_METERS
        }
        
        if distanceIsEnough {
            self.journey!.addPoint(JourneyPoint(latitude: value.latitude, longitude: value.longitude))
            drawPolyline()
        }
    }
    
    /// Draw the polyline on the mapView
    func drawPolyline (_ percentage:Double = 1) {
        var locations = self.journey!.points.map { $0.coordinate }
        
        //If we are playing the animation or simply if we want to show a partial polyline
        if ((percentage < 1) && (self.isPlaying)) {
            let partialTime:Double = self.journey!.startDate!.timeIntervalSince1970 +  percentage*self.journey!.duration
            locations = self.journey!.points.filter{$0.time.timeIntervalSince1970 <= partialTime}.map { $0.coordinate }
        }
        
        mapView.removeOverlays(self.mapView.overlays)
        
        let polyline = MKPolyline(coordinates: &locations, count: locations.count)
        mapView.add(polyline)
        
        if (!self.isRecording){
            self.updateMapRegion(polyline.boundingMapRect, Constants.MK_MAP_WHOLE_PATH_OFFSET)
        }
    }
    
    /// Does a zoom on the user location
    func zoomMapOnUser() {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(self.currentUserLocationCoordinate, Constants.MK_MAP_ZOOM, Constants.MK_MAP_ZOOM)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    /// Updates the mapRegion to see the full path
    ///
    /// - Parameters:
    ///   - rect: the region to show
    ///   - margin: the margin to be sure to see it well
    func updateMapRegion(_ rect:MKMapRect, _ margin:Double = 0) {
        var mRect:MKMapRect = rect
        mRect.origin.x -= margin
        mRect.origin.y -= 0.5*margin //the y here is adjusted conssidering the button a t the bottom of the interface (the actionButton)
        mRect.size.width += 2*margin
        mRect.size.height += 2*margin
        self.mapView.setRegion(MKCoordinateRegionForMapRect(mRect), animated: true)
    }
    
    /// Check the status and present the correct interface
    func checkMyStatus() {
        
        //If I'm adding a new journey
        if (self.journey == nil) {
            self.showAlertToTitleNewJourney()
            return
        }
        
        //show map and title
        self.mapView.isHidden = false
        self.navigationItem.title = self.journey!.title
        
        //If I'm not recording
        if (!self.isRecording) {
            
            //If current journey has no points, I'm ready to record
            if (self.journey?.points.count==0) {
                self.status = .ReadyToRecord
                return
            }else{
                if(!self.isPlaying) {
                    //I'm showing a recorded journey
                    self.status = .ReadyToPlay
                } else {
                    //I'm playing a journey
                    self.status = .Playing
                }
            }
        }else{
            //I'm back and I was in recording mode
            self.status = .Recording
        }
    }
    
    
    /// The play animation tick
    @objc func playAnimationTick() {
        self.animationTime += self.animationTimer!.timeInterval
        
        let cursor:Double = min(1, self.animationTime / Constants.PLAY_ANIMATION_DURATION)
        self.drawPolyline(cursor)
        
        if (self.animationTime >= Constants.PLAY_ANIMATION_DURATION ) {
            self.animationTimer!.invalidate()
            self.isPlaying = false
            self.checkMyStatus()
        }
    }
    
    /// Start and stop a journey tracking
    func toggleRecord(){
        self.isRecording = !self.isRecording
        
        if (self.isRecording) {
            self.escalateLocationServiceAuthorization()
            self.zoomMapOnUser()
            self.actionButton.startPulsing()
        } else {
            self.actionButton.stopPulsing()
            locationManager.stopUpdatingLocation()
            //We store the journey only if it has >1 points. It is a journey if you move
            if(self.journey!.points.count > 1){
                JourneyManager.sharedInstance.AddJourney(self.journey!)
            }
        }
        self.checkMyStatus()
    }
    
    //TODO: In addition to see a journey, you can play it in a 15 seconds animation
    func togglePlay() {
        self.isPlaying = !self.isPlaying
        
        if (self.isPlaying){
            self.animationTime = 0
            self.animationTimer = Timer.scheduledTimer(timeInterval: Constants.PLAY_ANIMATION_FRAMERATE, target: self, selector: #selector(playAnimationTick), userInfo: nil, repeats: true)
        }else{
            self.animationTimer?.invalidate()
        }
        
        self.checkMyStatus()
    }
    
    /// The method that responds to actionButton press
    ///
    /// - Parameter sender: the action button
    @IBAction func actionButtonPressed(_ sender:JourneyDiaryActionButton) {
        //We toggle record or play depending on its status
        switch sender.kind {
        case .Record:
            self.toggleRecord()
            break
        case .Play:
            self.togglePlay()
        }
    }
    
    //show the alert to enter  the new journey title
    func showAlertToTitleNewJourney() {
        let alertController = UIAlertController(title: Constants.LS("newJourneyTitle"), message: Constants.LS("newJourneyMsg"), preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField) in
            textField.placeholder = Constants.LS("newJourneyDefaultTitle")
        }
        let action = UIAlertAction(title: Constants.LS("newJourneyTitleEnter"), style: .default) { (alertAction) in
            let textField = alertController.textFields![0] as UITextField
            let titleTxt:String! = (textField.text ?? "").isEmpty ? Constants.LS("newJourneyDefaultTitle") : textField.text!
            self.journey = Journey(title:titleTxt)
            self.checkMyStatus()
        }
        let cancelAction = UIAlertAction(title: Constants.LS("cancel"), style: .cancel) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(action)
        alertController.addAction(cancelAction)
        self.present(alertController, animated:true, completion: nil)
    }
    
    //MARK: Location Manager Delegate
    
    //handling locations update
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        //check if locations is empty
        if !locations.isEmpty {
            let currentLocation:CLLocation = locations.last! as CLLocation
            //avoid considering cached values
            if ( abs(currentLocation.timestamp.timeIntervalSinceNow) < 15.0) {
                
                self.currentUserLocationCoordinate = currentLocation.coordinate
                
                //recenter the region if it is not changing already
                if (!self.regionIsChanging) {
                    self.mapView.setCenter(self.currentUserLocationCoordinate, animated: true)
                }
                
                if (self.isRecording && CLLocationCoordinate2DIsValid(self.currentUserLocationCoordinate)) {
                    self.addRecordCoordinate(self.currentUserLocationCoordinate)
                }
            }
        }
    }
    
    //MARK: MK MapView Delegate
    
    //handling locations update
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        //We can consider render other overlays further
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = self.isRecording ? Constants.RECORD_COLOR : Constants.PLAY_COLOR
            renderer.lineWidth = 3
            return renderer
        }
        
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        self.regionIsChanging = true
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.regionIsChanging = false
    }
}
